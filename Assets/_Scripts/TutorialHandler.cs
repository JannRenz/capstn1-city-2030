﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialHandler : MonoBehaviour
{
    public BuildStructure BuildStructureReference;
    public TextMeshProUGUI tooltipText;
    public GameObject tooltipArrow;
    public List<GameObject> buildings;
    public Button[] buttons;
    private bool TutorialStarted;
    private Buildings building;
    private int id;
    private float delayTimer;
    // Use this for initialization
    void Start()
    {
        tooltipText.gameObject.SetActive(false);
        tooltipArrow.gameObject.SetActive(false);
        
        StartCoroutine(CallFunctionWithDelay(0.9f));
        id = 1;
        BuildStructureReference.onBuildingSpawn += GetBuildingSpawn;

        buttons[0].gameObject.SetActive(false);
        buttons[1].gameObject.SetActive(false);
        buttons[2].gameObject.SetActive(false);
        buttons[3].gameObject.SetActive(false);
        buttons[4].gameObject.SetActive(false);
        buttons[5].gameObject.SetActive(false);
        buttons[6].gameObject.SetActive(false);
    }

    void GetBuildingSpawn(GameObject buildingSpawned)
    {
        buildings.Add(buildingSpawned);
    }

    // Update is called once per frame
    void Update()
    {
        if (TutorialStarted)
        {
            if (Input.GetMouseButton(0))
            {
                tooltipArrow.gameObject.SetActive(true);
            }

            if (buildings.Count != 0)
            {
                GameObject building = (id - 1 < buildings.Count) ? buildings[id - 1] : null;
                if (building != null)
                {
                    if (building.GetComponent<Buildings>().hasBuilt)
                    {
                        Debug.Log("House built");
                        ResumeTime();
                        id++;
                        TutorialStarted = false;
                        tooltipArrow.gameObject.SetActive(false);
                        StartCoroutine(CallFunctionWithDelay(0.5f));
                    }
                }
            }
        }
    }
    void TutorialFunction()
    {
        if (id == 1)
        {
            PauseTime();
            tooltipText.gameObject.SetActive(true);
            StartCoroutine(DisplayRollingText("In order to start growing your city, you must first cover the essentials. Build a house"));
            buttons[0].gameObject.SetActive(true);
            buttons[1].gameObject.SetActive(false);
            buttons[2].gameObject.SetActive(false);
            buttons[3].gameObject.SetActive(false);
            buttons[4].gameObject.SetActive(false);
            buttons[5].gameObject.SetActive(false);
            buttons[6].gameObject.SetActive(false);
            //building.hasBuilt = true;
        }
        else if (id == 2)
        {
            PauseTime();
            tooltipText.gameObject.SetActive(true);
            StartCoroutine(DisplayRollingText("In order to keep your inhabitants satisfied, you must also provide them food. Build a Barn"));
            buttons[0].gameObject.SetActive(false);
            buttons[1].gameObject.SetActive(true);
            buttons[2].gameObject.SetActive(false);
            buttons[3].gameObject.SetActive(false);
            buttons[4].gameObject.SetActive(false);
            buttons[5].gameObject.SetActive(false);
            buttons[6].gameObject.SetActive(false);
        }

        else if (id == 3)
        {
            PauseTime();
            tooltipText.gameObject.SetActive(true);
            StartCoroutine(DisplayRollingText("Oh no your water storage is decreasing! you must build a water pump. Build a Water Pump near a water area"));
            buttons[0].gameObject.SetActive(false);
            buttons[1].gameObject.SetActive(false);
            buttons[2].gameObject.SetActive(true);
            buttons[3].gameObject.SetActive(false);
            buttons[4].gameObject.SetActive(false);
            buttons[5].gameObject.SetActive(false);
            buttons[6].gameObject.SetActive(false);
        }

        else if (id == 4)
        {
            PauseTime();
            tooltipText.gameObject.SetActive(true);
            StartCoroutine(DisplayRollingText("You’ll also need electricity to keep your water pumps running. Build an Electricity Tower"));
            buttons[0].gameObject.SetActive(false);
            buttons[1].gameObject.SetActive(false);
            buttons[2].gameObject.SetActive(false);
            buttons[3].gameObject.SetActive(true);
            buttons[4].gameObject.SetActive(false);
            buttons[5].gameObject.SetActive(false);
            buttons[6].gameObject.SetActive(false);
        }
        else
        {
            ResumeTime();
            tooltipText.gameObject.SetActive(false);
            buttons[0].gameObject.SetActive(true);
            buttons[1].gameObject.SetActive(true);
            buttons[2].gameObject.SetActive(true);
            buttons[3].gameObject.SetActive(true);
            buttons[4].gameObject.SetActive(true);
            buttons[5].gameObject.SetActive(true);
            buttons[6].gameObject.SetActive(true);
        }
    }

    void PauseTime()
    {
        Time.timeScale = 0;
    }

    void ResumeTime()
    {
        Debug.Log("Time Resume");
        Time.timeScale = 1;
    }

    IEnumerator CallFunctionWithDelay(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        TutorialFunction();
    }

    IEnumerator DisplayRollingText(string text)
    {
        tooltipText.text = "";
        foreach (char letter in text)
        {
            tooltipText.text += letter;
            yield return new WaitForEndOfFrame();
        }

        //if (id == 1)
        //{
        TutorialStarted = true;
        //}

        //if (id == 2)
        //{
        //    TutorialStarted = true;
        //}

        //if (id == 3)
        //{
        //    TutorialStarted = true;
        //}

        //if (id == 4)
        //{
        //    TutorialStarted = true;
        //}

    }

}
