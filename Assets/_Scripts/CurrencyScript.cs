﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyScript : MonoBehaviour
{
    private static CurrencyScript instance;

    public static CurrencyScript Instance
    {
        get
        {
            if(!instance)
            {
                instance = GameObject.FindObjectOfType<CurrencyScript>();
                if(!instance)
                {
                    GameObject newInstance = new GameObject("CurrencyManager");
                    instance = newInstance.AddComponent<CurrencyScript>();
                }
            }
            return instance;
        }
    }

    #region
    public Text FoodText;
    public float FoodValue;

    public Text GoldText;
    public float GoldValue;

    public Text WaterText;
    public float WaterValue;

    public Text ElectricText;
    public float ElectricValue;

    public Text PopulationText;
    public float PopulationValue;

    public Text EduPopulationText;
    public float EduPopulationValue;

    public float foodRate;
    public float waterRate;

    public float goldRate;
    public float electricRate;

    public float populationRate;
    public float eduPopulationRate;
    #endregion

    void Update ()
    {
        FoodValue += foodRate * Time.deltaTime;
        FoodText.text = FoodValue.ToString("F0");

        GoldValue += goldRate * Time.deltaTime;
        GoldText.text = GoldValue.ToString("F0");

        WaterValue += waterRate * Time.deltaTime;
        WaterText.text = WaterValue.ToString("F0");

        ElectricValue += electricRate * Time.deltaTime;
        ElectricText.text = ElectricValue.ToString("F0");

        PopulationValue += populationRate * Time.deltaTime;
        PopulationText.text = PopulationValue.ToString("F0");

        EduPopulationValue += eduPopulationRate * Time.deltaTime;
        EduPopulationText.text = EduPopulationValue.ToString("F0");
    }
}
