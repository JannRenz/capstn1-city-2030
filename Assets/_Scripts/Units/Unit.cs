﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public string name;
    public float hp;
    public float att;
    public float attspd;
    public float mvmspd;
    public float rng;
    public float cost;
    public UnitDetection detection;
    public NavMeshAgent agent;

    public void Start()
    {
        detection = this.gameObject.GetComponentInChildren<UnitDetection>();
        agent = this.GetComponent<NavMeshAgent>();
        agent.speed = mvmspd;
        this.GetComponentInChildren<SphereCollider>().radius = rng;

    }
    public void LateUpdate()
    {
        if (hp <= 0)
        {
            Dead();
        }
    }
    public void Attack()
    {
        if (detection.enemy.layer == 10 || detection.enemy.layer == 13)
        {
            detection.enemy.gameObject.GetComponent<Unit>().hp -= this.gameObject.GetComponent<Unit>().att;
            //Debug.Log(detection.enemy.gameObject.GetComponent<Unit>().hp);
        }
        else if (detection.enemy.layer == 11)
        {
            detection.enemy.gameObject.GetComponent<Buildings>().hp -= this.gameObject.GetComponent<Unit>().att;
        }
    }
    public void Dead()
    {
        Destroy(this.gameObject);
    }
}
