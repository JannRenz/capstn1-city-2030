﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    //public List<Wave> SpawnPointss = new List<Wave>();
    public enum SpawnState { SPAWNING, WAITING, COUNTING };
    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform enemy;
        public int count;
        public float spawnRate;
    }

    public Wave[] waves;
    private int nextwave = 0;
    public int currentWave = 1;
    // public GameObject[] randomm;

    public Transform[] SpawnPoints;

    public float timeBetweenWaves = 5f;
    public float waveCountdown;

    private float searchCountdown = 1f;

    private SpawnState state = SpawnState.COUNTING;

    void Start()
    {
        waveCountdown = timeBetweenWaves;
    }
    void Update()
    {
        if (state == SpawnState.WAITING)
        {
            if (!EnemyIsAlive())
            {
                WaveCompleted();
                return;
                //Begin a new Round
            }
            else
            {
                return;
            }
            // Check if enemies are still alive
        }

        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextwave]));
                // Spawn Spawing wave
            }
        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }

    }

    void WaveCompleted()
    {
        Debug.Log("Wave Completed!");

        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if (nextwave + 1 > waves.Length - 1)
        {
            nextwave = 0;
            Debug.Log("All Waves Completed!! Looping ...");
        }
        else
        {
            nextwave++;
        }

    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("EnemyUnit") == null)
            {
                return false;
            }
        }
        return true;
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        //Debug.Log("Spawning Wave: " + _wave.name);

        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.spawnRate);

        }
        //Spawn

        state = SpawnState.WAITING;

        yield break;

    }

    void SpawnEnemy(Transform _enemy)
    {
        //Spawn enemy
        //Debug.Log("Spawning " + _enemy.name);

        if (SpawnPoints.Length == 0)
        {
            Debug.Log("No spawn points referenced.");
        }

        int SpawnIndex = Random.Range(0, SpawnPoints.Length);
        //int ObjectIndex = Random.Range(0, randomm.Length);
        GameObject enemiesWillSpawn = (GameObject)Instantiate(_enemy.gameObject, SpawnPoints[SpawnIndex].position, SpawnPoints[SpawnIndex].rotation);
    }
}
