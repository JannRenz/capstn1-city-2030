﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour {

    public Button ContinueButton;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void PauseGame()
    {
        Time.timeScale = 0;
        ContinueButton.gameObject.SetActive(true);
    }
    public void ContinueGame()
    {
        Time.timeScale = 1;
        ContinueButton.gameObject.SetActive(false);
    }
}
