﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanning : MonoBehaviour
{
    public float speed;

    public float MinX;
    public float MaxX;

    public float MinY;
    public float MaxY;

    public float MinZ;
    public float MaxZ;

    

    void Update()
    {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 pos = this.transform.position;
        if (/*mousePosition.x > Screen.width - 20 ||*/ Input.GetKey(KeyCode.D))
        {
            //this.transform.Translate(Vector3.right * speed * Time.deltaTime); // right
            
            pos.x += speed * Time.deltaTime;
        }
        if (/*mousePosition.x < 20 ||*/ Input.GetKey(KeyCode.A))
        {
            //this.transform.Translate(Vector3.left * speed * Time.deltaTime); // left
            pos.x -= speed * Time.deltaTime;
        }
        if (/*mousePosition.y > Screen.height - 20 ||*/ Input.GetKey(KeyCode.W)  )
        {
            //this.transform.Translate(Vector3.up * speed * Time.deltaTime); // up
            //this.transform.Translate(Vector3.forward * speed * Time.deltaTime); // up
            pos.z += speed * Time.deltaTime;
        }
        if (/*mousePosition.y < 20 ||*/ Input.GetKey(KeyCode.S))
        {
            //this.transform.Translate(Vector3.down * speed * Time.deltaTime); // down
            //this.transform.Translate(Vector3.back * speed * Time.deltaTime); // down
            pos.z -= speed * Time.deltaTime;
        }

        //pos.y = Mathf.Clamp(transform.position.y, 32f, 33f);
        //transform.position = pos;
        //pos.y = Mathf.Clamp(pos.y,MinY,MaxY);
        pos.x = Mathf.Clamp(pos.x, MinX, MaxX);
        pos.z = Mathf.Clamp(pos.z, MinZ, MaxZ);
        //this.transform.position = new Vector3(
        //    Mathf.Clamp(transform.position.x, MinX, MaxX),
        //    Mathf.Clamp(transform.position.y, MinY, MaxY),
        //    Mathf.Clamp(transform.position.z, MinZ, MaxZ)
        //    );
        transform.localPosition = pos;

    }
}
