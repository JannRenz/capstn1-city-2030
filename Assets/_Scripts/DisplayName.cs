﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayName : MonoBehaviour
{
    public GameObject panel;

    public TextMeshProUGUI buildingName;
    public TextMeshProUGUI buildingFood;
    public TextMeshProUGUI buildingGold;
    public TextMeshProUGUI buildingWater;
    public TextMeshProUGUI buildingElec;
    public TextMeshProUGUI buildingPop;
    public TextMeshProUGUI buildingEdu;

    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}
    public void DisplayButtonName(Buildings building)
    {
        panel.gameObject.SetActive(true);
        buildingName.text = building.name.ToString();
        buildingFood.text = building.foodCost.ToString();
        buildingGold.text = building.goldCost.ToString();
        buildingWater.text = building.waterCost.ToString();
        buildingElec.text = building.electricCost.ToString();
        buildingPop.text = building.populationCost.ToString();
        buildingEdu.text = building.eduPopulationCost.ToString();
        //Debug.Log(Name);
    }
    
    public void HideButtonName()
    {
        panel.gameObject.SetActive(false);
    }
}
