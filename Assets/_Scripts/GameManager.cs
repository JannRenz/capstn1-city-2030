﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    GameObject Unit;
    [SerializeField]
    GameObject SpawnPoint;
    [SerializeField]
    GameObject EnemySpawnPoint;
    //[SerializeField]
    //int Waves;
    public bool barracksBuilt = false;
    public UnitRequirements unitReq;
    public CurrencyScript currency;
    public 
	// Use this for initialization
	void Start ()
    {
        GameObject unit = Instantiate(Unit, EnemySpawnPoint.transform.position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SpawnUnit(GameObject Unit)
    {
        if(Unit.name == "Police")
        {
            if(barracksBuilt && currency.FoodValue >= Unit.GetComponent<Unit>().cost)
            {
                Instantiate(Unit, SpawnPoint.transform.position, Quaternion.identity);
                currency.FoodValue -= Unit.GetComponent<Unit>().cost;
            }
        }
        else
        {
            Instantiate(Unit, SpawnPoint.transform.position, Quaternion.identity);
        }
    }
}
