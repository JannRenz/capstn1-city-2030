﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InGameTimeManager : MonoBehaviour
{
    private const int TimeScale = 90;

    private TextMeshProUGUI clockText;
    private TextMeshProUGUI dayText;
    private TextMeshProUGUI monthText;
    private TextMeshProUGUI yearText;

    private double minute, hour, day, second, month, year;
    // Use this for initialization
    void Start()
    {
        day = 1;
        month = 1;
        year = 2018;

        clockText = GameObject.FindWithTag("Clock").GetComponent<TextMeshProUGUI>();
        dayText = GameObject.FindWithTag("Day").GetComponent<TextMeshProUGUI>();
        monthText = GameObject.FindWithTag("Month").GetComponent<TextMeshProUGUI>();
        yearText = GameObject.FindWithTag("Year").GetComponent<TextMeshProUGUI>();

        CalculateMonth();
    }

    // Update is called once per frame
    void Update()
    {
        CalculateTime();
    }

    void TextCallFunction()
    {
        dayText.text = "Day: " + day;
        clockText.text = "Time: " + hour + ":" + minute;
        monthText.text = "Month: " + month;
        yearText.text = "Year: " + year;
    }

    void CalculateTime()
    {
        /** Current algorithm is set to 1min realtime = 1hr gametime. Change timescale to alter */
        second += Time.deltaTime * TimeScale;

        if (second >= 60)
        {
            minute++;
            second = 0;
            TextCallFunction();
        }
        else if (minute >= 60)
        {
            hour++;
            minute = 0;
            TextCallFunction();
        }
        else if (hour >= 24)
        {
            day++;
            hour = 0;
            TextCallFunction();
        }
        else if (day >= 28)
        {
            CalculateMonth();
        }
        else if(month >= 13)
        {
            month = 1;
            year++;
            TextCallFunction();
        }
    }

    void CalculateMonth()
    {
        /** For the months with 31 days*/
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
        {
            if(day >= 32)
            {
                month++;
                day = 1;
                TextCallFunction();
            }
        }
        /** For February*/
        if (month == 2)
        {
            if(day >= 29)
            {
                month++;
                day = 1;
            }
        }
        /** For the months with 30 days*/
        if(month == 4 || month == 6 || month == 9 || month == 11)
        {
            if (day >= 31)
            {
                month++;
                day = 1;
                TextCallFunction();
            }
        }
    }
}
