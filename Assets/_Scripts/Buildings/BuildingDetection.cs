﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDetection : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Building")
        {
            this.gameObject.GetComponent<Buildings>().canBuild = false;
            Debug.Log("can't build");
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Building")
        {
            this.gameObject.GetComponent<Buildings>().canBuild = true;
            Debug.Log("can build");
        }
    }

}
