﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDetection : MonoBehaviour
{
    public bool canBuild = false;

    void Update()
    {
        Collider[] waterDetect = Physics.OverlapSphere(transform.position, 10, LayerMask.GetMask("Water"));
        if (!canBuild && waterDetect.Length != 0)
        {
            canBuild = true;
            Debug.Log("You are Near Water");
        }
        else if (canBuild && waterDetect.Length == 0)
        {
            canBuild = false;
            Debug.Log("Need to be near water");
        }

            
    }
    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Water")
    //    {
    //        canBuild = true;
    //        Debug.Log("You are Near Water");
    //    }
    //}
    //void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.tag == "Water")
    //    {
    //        canBuild = false;
    //        Debug.Log("Need to be near water");
    //    }
    //}
}
