﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public Vector3 TargetLocation { set; get; }
    public Transform LockOn { set; get; }
    public bool isLockOnTarget { set; get; }

    private bool isLaunch = false;
    public float damage;
    public float speed;

    public Projectile()
    {
        LockOn = null;
        isLockOnTarget = false;
        damage = 1;
        speed = 6;
    }
    protected virtual void Update()
    {
        if (!isLaunch)
        {
            return;
        }
        if (isLockOnTarget && LockOn)
        {
            TargetLocation = LockOn.position;
        }

        transform.position = Vector3.MoveTowards(transform.position, TargetLocation, speed * Time.deltaTime);
    }
    public virtual void Launch(Vector3 targetLocation)
    {
        isLaunch = true;
        TargetLocation = targetLocation;
        //damage = dmg;
    }
    protected virtual void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        other.gameObject.GetComponent<Unit>().hp -= damage;
    }
}
