﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicProjectile : Projectile {

    protected override void Update()
    {
        base.Update();
        transform.LookAt(TargetLocation);
        Destroy(gameObject, 2);
    }
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyUnit")
        {
            Debug.Log("LJ");
            base.OnTriggerEnter(other);

        }
    }
}
