﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    public enum States
    {
        BUILDING,
        BUILD,
    }

    protected float lastSearch;
    protected float refreshRate = 0.10f;

    protected float lastAttack;
    public float cooldown = 1.0f;

    public float range = 5.0f;
    public float cost;
    public float upgradeCost;
    public float constructTime;
    public float damage;

    //protected Transform turrentTransform;
    protected Transform myTransform;
    public Transform target;
    public GameObject projectilePrefab;
    public GameObject upgradeTo;
    public States state;

    private void Start()
    {
        myTransform = this.transform;
        //turrentTransform = transform.Find("Turret");
        state = States.BUILD;
    }

    private void Update()
    {
        //if the tower is ready to attack
        switch (state)
        {
            case States.BUILDING:
                break;
            case States.BUILD:
                if (Time.time - lastAttack > cooldown)
                {
                    //refresh every 0.10f to find a target
                    if (Time.time - lastSearch > refreshRate)
                    {
                        lastSearch = Time.time;
                        //Get Target
                        target = GetNearestEnemy();
                        if (target != null)
                        {
                            Attack();
                            //LookAtEnemy();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private Transform GetNearestEnemy()
    {
        Collider[] allEnemies = Physics.OverlapSphere(transform.position, range, LayerMask.GetMask("Enemy"));
        if (allEnemies.Length != 0)
        {
            int closestIndex = 0;
            float nearestDist = Vector3.SqrMagnitude(transform.position - allEnemies[0].transform.position);
            for (int i = 1; i < allEnemies.Length; i++)
            {
                float distance = Vector3.SqrMagnitude(transform.position - allEnemies[i].transform.position);
                if (distance < nearestDist)
                {
                    nearestDist = distance;
                    closestIndex = i;
                }
            }
            return allEnemies[closestIndex].transform;
        }
        return null;
    }
    protected virtual void Attack()
    {
        lastAttack = Time.time;
    }
    //protected void LookAtEnemy()
    //{
    //    if (turrentTransform != null)
    //    {
    //        Vector3 dir = target.position - transform.position;

    //        Quaternion lookAt = Quaternion.LookRotation(dir);

    //        turrentTransform.rotation = Quaternion.Euler(0, lookAt.eulerAngles.y, 0);

    //        turrentTransform.LookAt(target);
    //    }
    //}
}
