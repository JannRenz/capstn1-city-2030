﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTower : Tower {

    protected override void Attack()
    {
        base.Attack();
        Debug.DrawRay(transform.position, target.position - transform.position, Color.red);

        GameObject arrow = (GameObject)Instantiate(projectilePrefab, transform.position, Quaternion.identity);
        arrow.GetComponent<Projectile>().damage = damage;
        arrow.GetComponent<Projectile>().Launch(target.position);

    }
}
