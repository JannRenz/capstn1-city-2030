﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class BuildRoad : MonoBehaviour {

    public GameObject Road;
    

	void Start ()
    {
		
	}
	
	void Update ()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Ground"), QueryTriggerInteraction.Ignore))
        {
            Debug.Log(hit.collider.gameObject.name);
            Vector3 towerPos = hit.point;
            
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && hit.collider.tag == "Ground")
            {
                Instantiate(Road);
                Road.transform.position = SnapToGrid(towerPos);
            }
        }
    }

    Vector3 SnapToGrid(Vector3 towerObject)
    {
        return new Vector3(Mathf.Round(towerObject.x), towerObject.y + 0.5f, Mathf.Round(towerObject.z));
    }
    public void SelectRoad()
    {

    }
}
