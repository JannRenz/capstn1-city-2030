﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buildings : MonoBehaviour
{

    public new string name;
    public float hp;
    public float goldCost;
    public float foodCost;
    public float waterCost;
    public float electricCost;
    public float populationCost;
    public float eduPopulationCost;
    public float constructionTime;
    public bool hasBuilt;
    public bool hasEffect;
    public bool canBuild = true;
    public Button corresButton;
    public CurrencyScript currency;
    public string ButtonName;

    void Awake()
    {
        corresButton = GameObject.Find(ButtonName).GetComponent<Button>();
    }
	void Start ()
    {
        currency = CurrencyScript.Instance; 
    }
	
	public virtual void Update ()
    {
        
        if (corresButton && BuildingCost())
        {
            Debug.Log("interactable");
            corresButton.interactable = true;
        }
        else if (corresButton && !BuildingCost())
        {
            Debug.Log("not interactable");
            corresButton.interactable = false;
        }
    }
    public virtual void BuildingEffect()
    {
        currency = GameObject.Find("CurrencyManager").GetComponent<CurrencyScript>();
    }
    public virtual bool BuildingRequirement()
    {
        Debug.Log("Requirement Not Met");

        //corresButton.interactable = false;
        return false;
    }
    public virtual bool BuildingCost()
    {
        Debug.Log("Insuficient Resources");
        return false;
    }
}
