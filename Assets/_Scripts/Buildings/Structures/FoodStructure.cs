﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodStructure : Buildings {

	void Start ()
    {
		
	}

    public override void Update()
    {
        base.Update();
        BuildingEffect();
        //BuildingRequirement();
    }

    public override void BuildingEffect()
    {
        base.BuildingEffect();
        if (hasBuilt && hasEffect)
        {
            currency.foodRate += 0.3f;
            hasEffect = false;
            //hasBuilt = false;
        }
    }
    public override bool BuildingRequirement()
    {
        if (canBuild)
        {
            currency.GoldValue -= goldCost;
            return true;
        }
        else
            return false;
        
    }
    public override bool BuildingCost()
    {
        currency = GameObject.FindGameObjectWithTag("CurrencyManager").GetComponent<CurrencyScript>();
        if (currency.GoldValue >= goldCost )
        {
            
            return true;
        }
        else
        {
            return base.BuildingCost();
        }
    }
}
