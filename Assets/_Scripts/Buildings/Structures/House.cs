﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : Buildings
{

    public BuildStructure BuildStructureReference;

    void Start ()
    {
        BuildStructureReference = GameObject.Find("BuildingManager").GetComponent<BuildStructure>();
        
        BuildStructureReference.onBuildingSpawn += ReduceGoldCurrency;
    }

    void ReduceGoldCurrency(GameObject buildingSpawned)
    {
        //currency.GoldValue -= buildingSpawned.GetComponent<Buildings>().goldCost;
    }
	
	public override void Update ()
    {
        base.Update();
        BuildingEffect();
        //BuildingRequirement();
    }
    public override void BuildingEffect()
    {
        base.BuildingEffect();
        if (hasBuilt && hasEffect)
        {
            currency.populationRate += 0.3f;
            currency.electricRate -= 0.1f;
            currency.waterRate -= 0.1f;
            hasEffect = false;
            Debug.Log(hasBuilt);
            //hasBuilt = false;
        }
    }
    public override bool BuildingRequirement()
    {
        if (canBuild)
        {
            currency.GoldValue -= goldCost;
            return true;
        }
        else
            return false;
        
    }
    public override bool BuildingCost()
    {
        //currency = GameObject.FindGameObjectWithTag("CurrencyManager").GetComponent<CurrencyScript>();
        if (CurrencyScript.Instance.GoldValue >= goldCost)
        {         
            return true;
        }
        else
        {
            return base.BuildingCost();
        }
    }
}
