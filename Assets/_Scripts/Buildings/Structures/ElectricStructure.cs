﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricStructure : Buildings
{

    public override void Update()
    {
        base.Update();
        BuildingEffect();
        //BuildingRequirement();
    }
    public override void BuildingEffect()
    {
        base.BuildingEffect();
        if (hasBuilt && hasEffect)
        {
            currency.electricRate += 0.3f;
            hasEffect = false;
            //hasBuilt = false;
        }
    }
    public override bool BuildingRequirement()
    {
        if (canBuild)
        {
            currency.GoldValue -= goldCost;
            return true;
        }
        else
            return false;
        
    }
    public override bool BuildingCost()
    {
        currency = GameObject.FindGameObjectWithTag("CurrencyManager").GetComponent<CurrencyScript>();
        if (currency.GoldValue >= goldCost)
        {
            
            return true;
        }
        else
        {
            return base.BuildingCost();
        }
    }
}
