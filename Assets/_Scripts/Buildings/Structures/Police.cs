﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Police : Buildings {

	// Use this for initialization
	void Start ()
    {
		
	}

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        BuildingEffect();
        //BuildingRequirement();
    }

    public override void BuildingEffect()
    {
        base.BuildingEffect();
    }
    public override bool BuildingRequirement()
    {
        if (canBuild)
        {
            currency.GoldValue -= goldCost;
            currency.ElectricValue -= electricCost;
            currency.WaterValue -= waterCost;
            currency.FoodValue -= foodCost;
            return true;
        }
        else
            return false;
        
    }
    public override bool BuildingCost()
    {
        currency = GameObject.FindGameObjectWithTag("CurrencyManager").GetComponent<CurrencyScript>();
        if (currency.GoldValue >= goldCost && currency.ElectricValue >= electricCost && currency.WaterValue >= waterCost && currency.PopulationValue>= populationCost && currency.FoodValue >= foodCost)
        {
            
            return true;
        }
        else
        {
            return base.BuildingCost();
        }
    }
}
