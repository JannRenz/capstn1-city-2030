﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hospital : Buildings {

	// Use this for initialization
	void Start ()
    {
		
	}

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        BuildingEffect();
        //BuildingRequirement();
    }

    public override void BuildingEffect()
    {
        base.BuildingEffect();
        if (hasBuilt)
        {
            Debug.Log("EffectOn");
            currency.goldRate += 0.3f;
            hasBuilt = false;
        }
    }
    public override bool BuildingRequirement()
    {
        if (canBuild)
        {
            currency.GoldValue -= goldCost;
            currency.ElectricValue -= electricCost;
            currency.WaterValue -= waterCost;
            return true;
        }
        else
            return false;
        
    }
    public override bool BuildingCost()
    {
        currency = GameObject.FindGameObjectWithTag("CurrencyManager").GetComponent<CurrencyScript>();
        if (currency.GoldValue >= goldCost && currency.ElectricValue >= electricCost && currency.WaterValue >= waterCost && currency.EduPopulationValue >= eduPopulationCost)
        {
            
            return true;
        }
        else
        {
            return base.BuildingCost();
        }
    }
}
