﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterStructure : Buildings
{
    public override void Update()
    {
        base.Update();
        BuildingEffect();
        //BuildingRequirement();
    }
    public override void BuildingEffect()
    {
        base.BuildingEffect();
        //currency = GameObject.FindGameObjectWithTag("CurrencyManager").GetComponent<CurrencyScript>();
        currency = GameObject.Find("CurrencyManager").GetComponent<CurrencyScript>();
        if (hasBuilt && hasEffect)
        {
            currency.waterRate += 0.3f;
            hasEffect = false;
            //hasBuilt = false;
        }
    }
    public override bool BuildingRequirement()
    {
        if (this.gameObject.GetComponentInChildren<WaterDetection>().canBuild && canBuild)
        {
            currency.GoldValue -= goldCost;
            return true;
        }
        else
            return base.BuildingRequirement();
        
    }
    public override bool BuildingCost()
    {
        currency = GameObject.FindGameObjectWithTag("CurrencyManager").GetComponent<CurrencyScript>();
        if (currency.GoldValue >= goldCost)
        {
            return true;
        }
        else
        {
            return base.BuildingCost();
        }
    }
}
