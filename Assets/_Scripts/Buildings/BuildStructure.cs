﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildStructure : MonoBehaviour
{

    public GameObject currencyManager;
    //public GameObject[] towers;
    public GameObject towerToBuild;
    public GameObject builder;
    //public Text numBuilder;
    public LayerMask layer;

    public float builders = 3;
    public float buildingTime;
    public Vector3 gridSize;

    bool isBuilding;
    int towerNum = 0;
    float buildingCost;
    bool buildNow = false;

    public GameManager gameManager;
    WaterDetection waterDetect;

    public delegate void OnBuildingSpawn(GameObject buildingSpawned);
    public OnBuildingSpawn onBuildingSpawn;
            
	void Update ()
    {
        if (towerToBuild != null)
        {
            BuildPreview();
            CancelSelection();
              
        }	
	}

    Vector3 SnapToGrid(Vector3 towerObject)
    {
        return new Vector3(Mathf.Round(towerObject.x / gridSize.x) * gridSize.x, towerObject.y + towerToBuild.transform.position.y, Mathf.Round(towerObject.z / gridSize.z) * gridSize.z);
    }

    void BuildPreview()
    {
        if (towerToBuild != null && buildNow)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layer, QueryTriggerInteraction.Ignore))
            {
                
                Vector3 towerPos = hit.point;
                towerToBuild.transform.position = SnapToGrid(towerPos);
                
            }
            if (towerToBuild != null && hit.collider != null)
            {
                if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && hit.collider.tag == "Ground")
                {
                    isBuilding = true;
                    #region
                    /*if (towerToBuild.GetComponent<Buildings>().name == "WaterStructure" && towerToBuild.GetComponentInChildren<WaterDetection>(true).canBuild == true)
                    {
                        if (isBuilding && towerToBuild.GetComponent<Buildings>() != null && currencyManager.GetComponent<CurrencyScript>().GoldValue >= towerToBuild.GetComponent<Buildings>().goldCost)
                        {
                            currencyManager.GetComponent<CurrencyScript>().GoldValue -= towerToBuild.GetComponent<Buildings>().goldCost;
                            StartCoroutine(Constructing());
                        }
                    }
                    else if (towerToBuild.GetComponent<Buildings>().name == "WaterStructure" && towerToBuild.GetComponentInChildren<WaterDetection>(true).canBuild == false)
                    {
                        return;
                    }
                    else if (isBuilding && towerToBuild.GetComponent<Buildings>() != null && currencyManager.GetComponent<CurrencyScript>().GoldValue >= towerToBuild.GetComponent<Buildings>().goldCost)
                    {
                        currencyManager.GetComponent<CurrencyScript>().GoldValue -= towerToBuild.GetComponent<Buildings>().goldCost;
                        StartCoroutine(Constructing());
                    }


                    else if (isBuilding && towerToBuild.GetComponent<Tower>() != null)
                    {
                        currencyManager.GetComponent<CurrencyScript>().GoldValue -= towerToBuild.GetComponent<Tower>().cost;
                        StartCoroutine(Constructing());
                    }*/
                    #endregion
                    if (isBuilding && towerToBuild != null && towerToBuild.GetComponent<Buildings>().BuildingRequirement() && towerToBuild.GetComponent<Buildings>().canBuild)
                    {
                        if (onBuildingSpawn != null)
                        {
                            onBuildingSpawn.Invoke(towerToBuild);
                        }

                        StartCoroutine(Constructing());
                    }
                }
                else if (Input.GetMouseButtonDown(0) && EventSystem.current.IsPointerOverGameObject())
                {
                    return;
                }
            }
        }
    }
    IEnumerator Constructing()
    {
        Debug.Log("Building");
        buildNow = false;
        buildingTime = towerToBuild.GetComponent<Buildings>().constructionTime;
        towerToBuild.GetComponent<Buildings>().hasBuilt = true;
        towerToBuild.GetComponent<Buildings>().hasEffect = true;
        towerToBuild = null;
        yield return new WaitForSecondsRealtime(buildingTime);
        isBuilding = false;
        Debug.Log("Complete");
    }
  
    public void SelectBuilding(GameObject building)
    {
        if (towerToBuild == null && building.GetComponent<Buildings>().BuildingCost() && EventSystem.current.IsPointerOverGameObject())
        {
            towerToBuild = Instantiate(building) as GameObject;
            
            buildNow = true;
        }
    }
    void CancelSelection()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Destroy(towerToBuild.gameObject);
            towerToBuild = null;
            buildNow = false;
        }
    }
}
